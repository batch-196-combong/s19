console.log('hello world');

// Conditional Statements
// Conditional statements allow us to perform tasks based on a condition.

let num1 = 0;

// If statement - proceeds when the IF condition is given.

/*
	if(condition){
		task/code to perform
	}
*/

if(num1===0){
	console.log("The value of num1 is 0");
}

num1 = 25;

if(num1===0){
	console.log("The value of num1 is 0");
}

let city = 'New York';

if(city === 'New Jersey'){
	console.log('Welcome to New Jersey');
} else {
	console.log('Welcome to New York');
}

if(num1 < 20){
	console.log('num1\'s value is less than 20');
} else {
	console.log('num1\'s value is more than 20');
}

function cityChecker(city){
	if(city === 'New York'){
		console.log('Welcome to the Empire State');
	} else {
		console.log('You are not in New York!')
	}
}

cityChecker('New York');
cityChecker('Los Angeles');

// Mini Activity
function budgetChecker(budget){
	if(budget <= 40000){
	console.log("You're still within the budget");
} else {
	console.log("You're currently over budget");
}
}
budgetChecker(40000);
budgetChecker(50000);

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return 'Not a typhoon yet'
	} else if (windSpeed <= 61){
		return 'Tropical Depression Detected'
	} else if (windSpeed >= 62 && windSpeed <= 88){
		return 'Typhoon Storm Detected'
	} else {
		return "Typhoon Detected"
	}
}

let typhoonMsg1 = determineTyphoonIntensity(29)
let typhoonMsg2 = determineTyphoonIntensity(62)
let typhoonMsg3 = determineTyphoonIntensity(61)
let typhoonMsg4 = determineTyphoonIntensity(88)

console.log(typhoonMsg1)
console.log(typhoonMsg2)
console.log(typhoonMsg3)
console.log(typhoonMsg4)

// Truthy and Falsy values

// In Js, there ar values that are considered "Truthy", which means in a boolean context, like determining an if condition, it is considered true. 

// 1. true
// 2. 1
// 3. []
if(1){
	console.log('1 is truthy')
}

if([]){
	console.log('[] is truthy')
}

//Falsy values are values considered 'False' in a boolean context, like determining an if condition

// 1. false
// 2. 0
if(0){
	console.log('0 is falsy');
}
// 3. undefined
if(undefined){
	console.log('undefined is not falsy')
} else {
	console.log('undefined is falsy')
}

// conditional tenary operator
// tenary operator is used as a shorter alternative to if else statements
// It is also able to implicitly return a value. Meaning it does not have to use the return keyword to return a value.

// syntax:
// (condition) ? ifTrue : ifFalse;

let age = 17;
let result = age < 18 ? "You are underaged." : "You are at the legal age"
console.log(result)

// switch statements
// evaluate an expression and match the expression to a case clause.
// an expression will be compared aainst different cases. Then, we will b able to run code IF the expression being evaluated matches a case. 
//It is used alternatively from an if-else statement. However, if-else statements provides more complexity in its conditions. 

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

// switch statement to evaluate the current day and show a message to tell the user the color of the day.

switch(day) {
	case 'monday':
		console.log('The color of the day is red')
		break;
	case 'tuesday' :
		console.log('The color of the day is orange')
		break;
	case 'wednesday' :
		console.log('The color of the day is yellow')
		break;
	case 'thursday' :
		console.log('The color of the day is blue')
		break;
	case 'friday' :
		console.log('The color of the day is green')
		break;
	case 'saturday' :
		console.log('The color of the day is indigo')
		break;
	case 'sunday' :
		console.log('The color of the day is violet')
		break;
	default :
		console.log('Please enter a valid day')
		break;
}

// Try Catch  Finally
// We use try- catch finally statements to catch errors, display and inform about he error and continue the code instead of stopping. 

try{
	alert(determineTyphoonIntensity(61))
} catch (error) {
	console.log(error.message);
} finally {
	alert('Intensity update will show in a new alert')
}