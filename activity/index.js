console.log('Hello World')

let username, password, role;

function login(username, password, role){
	username = prompt('username: ').toLowerCase();
	password = prompt('password: ').toLowerCase();
	role = prompt('role: ').toLowerCase();

	if(username == '' || username == 'null') {
		alert('Input must not be empty');
	} else if(password == '' || password == 'null'){
		alert('Input must not be empty');
	} else if(role == '' || role == 'null'){
		alert('Input must not be empty');
	} else {
		switch (role){
			case 'admin' :
				console.log('Welcome back to the class portal, admin');
				break;
			case 'teacher' :
				console.log('Thank you for logging in teacher');
				break;
			case 'student' :
				console.log('Welcome to the class portal, student');
				break;
			default :
				console.log('Role out of range');
				break;
		}

	}
}

login();

function checkAverage(grade1, grade2, grade3, grade4){
	let average = (grade1 + grade2 + grade3 + grade4)/4;
	average = Math.round(average);
		if(average <= 74){
			console.log('Hello student, your average is ' + average+ '. The equivalent is F.');
		} else if (average >= 75 && average <= 79){
			console.log('Hello student, your average is ' + average+ '. The equivalent is D.');
		} else if (average >= 80 && average <= 84){
			console.log('Hello student, your average is ' + average+ '. The equivalent is C.');
		} else if (average >= 85 && average <= 89){
			console.log('Hello student, your average is ' + average+ '. The equivalent is B.');
		} else if (average >= 90 && average <= 95){
			console.log('Hello student, your average is ' + average+ '. The equivalent is A.');
		} else if (average > 96){
			console.log('Hello student, your average is ' + average+ '. The equivalent is A+.');
		}

}
